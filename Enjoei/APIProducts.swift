//
//  APIProducts.swift
//  Enjoei
//
//  Created by Fernando Martinez on 5/7/16.
//  Copyright © 2016 fernandodev. All rights reserved.
//

import Foundation

class APIProducts {
    let productsURL = NSURL(string: "https://gist.githubusercontent.com/caironoleto/9cd18d9642a7d5e8eef0/raw/dbe726570f063e2be2b353fd35d48ff9f9180b52/_products.json")!

    func products(whenSuccess success: (products: [Product]) -> Void, failure: (error: NSError) -> Void, completion: () -> Void) {
        let request = NSURLRequest(URL: productsURL)
        NSURLSession.sharedSession().dataTaskWithRequest(
            request,
            completionHandler: { data, response, error in
                if let data = data where error == nil {
                    if let json = (try? NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers)) as? [String:AnyObject] {

                        if let productsArray = json["products"] as? [AnyObject] {
                            let products: [Product] = productsArray.ext_transformJsonAray()
                            success(products: products)
                        } else {
                            failure(error: NSError(domain: "Can't parse JSON", code: -1, userInfo: nil))
                        }
                    } else {
                        failure(error: NSError(domain: "Can't parse JSON", code: -1, userInfo: nil))
                    }
                } else if let error = error {
                    failure(error: error)
                } else {
                    failure(error: NSError(domain: "Unknown server error", code: -2, userInfo: nil))
                }

                completion()
            }
        ).resume()
    }
}
