//
//  UIRefreshControl.swift
//  Enjoei
//
//  Created by Fernando Martinez on 5/7/16.
//  Copyright © 2016 fernandodev. All rights reserved.
//

import UIKit

extension UIRefreshControl {
    func beginRefreshingManually() {
        if let scrollView = superview as? UIScrollView {
            scrollView.setContentOffset(CGPoint(x: 0, y: scrollView.contentOffset.y - frame.height), animated: true)
        }
        beginRefreshing()
    }
}