//
//  UIImageView.swift
//  Enjoei
//
//  Created by Fernando Martinez on 5/7/16.
//  Copyright © 2016 fernandodev. All rights reserved.
//

import Foundation
import UIKit

extension UIImageView {

    func setImage(withURL url: NSURL) {
        let request = NSURLRequest(URL: url)
        NSURLConnection.sendAsynchronousRequest(
            request,
            queue: NSOperationQueue.mainQueue(),
            completionHandler: { response, data, error in
                if let data = data where error == nil {
                    self.image = UIImage(data: data)
                }
            }
        )
    }
}