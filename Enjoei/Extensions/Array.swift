//
//  Array.swift
//  Enjoei
//
//  Created by Fernando Martinez on 5/8/16.
//  Copyright © 2016 fernandodev. All rights reserved.
//

import Foundation

extension Array {

    func ext_transformJsonAray<T: JSONDeserialization>() -> [T] {
        var array: [T] = []

        for index in 0...(self.count-1) {
            if let e = self[index] as? [String:AnyObject] {
                array.append(T(json: e))
            }
        }

        return array
    }
}
