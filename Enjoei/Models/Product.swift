//
//  Product.swift
//  Enjoei
//
//  Created by Fernando Martinez on 5/7/16.
//  Copyright © 2016 fernandodev. All rights reserved.
//

import Foundation

struct Product: JSONDeserialization {
    var id: Int
    var discountPercentage: Int
    var originalPrice: Int
    var price: Int
    var title: String
    var content: String
    var likesCount: Int
    var defaultPhotoUrl: NSURL?

    init(json: [String : AnyObject]) {
        id = (json["id"] as? Int) ?? 0
        discountPercentage = (json["discount_percentage"] as? Int) ?? 0
        originalPrice = (json["original_price"] as? Int) ?? 0
        price = (json["price"] as? Int) ?? 0
        title = (json["title"] as? String) ?? ""
        content = (json["content"] as? String) ?? ""
        likesCount = (json["likes_count"] as? Int) ?? 0
        if let photo = json["default_photo"] as? String {
            defaultPhotoUrl = NSURL(string: photo)
        }
    }
}
