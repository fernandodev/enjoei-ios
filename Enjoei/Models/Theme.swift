//
//  Theme.swift
//  Enjoei
//
//  Created by Fernando Martinez on 5/6/16.
//  Copyright © 2016 fernandodev. All rights reserved.
//

import UIKit

struct Theme {

    static var bgViewColor = UIColor(red: 0.96, green: 0.95, blue: 0.94, alpha: 1)
    static var textPlaceholderColor = UIColor(red: 0.79, green: 0.77, blue: 0.74, alpha: 1)
    static var textColor = UIColor(red: 0.49, green: 0.47, blue: 0.46, alpha: 1)
    static var textBoldColor = UIColor(red: 0.356, green: 0.345, blue: 0.33, alpha: 1)

    static var fontNovaRegularName = "ProximaNova-Regular"
    static var fontNovaLightItName = "ProximaNova-LightIt"
    static var fontNovaBoldName = "ProximaNova-Bold"
    static var fontNovaLight = "ProximaNova-Light"
    static var fontSemiBold = "ProximaNova-SemiBold"
}
