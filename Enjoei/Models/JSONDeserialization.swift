//
//  JSONDeserialization.swift
//  Enjoei
//
//  Created by Fernando Martinez on 5/8/16.
//  Copyright © 2016 fernandodev. All rights reserved.
//

import Foundation

protocol JSONDeserialization {
    init(json: [String:AnyObject])
}