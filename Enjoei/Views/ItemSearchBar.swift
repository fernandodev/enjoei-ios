//
//  ItemSearchBar.swift
//  Enjoei
//
//  Created by Fernando Martinez on 5/7/16.
//  Copyright © 2016 fernandodev. All rights reserved.
//

import Foundation
import UIKit

class ItemSearchBar: UISearchBar {

    override func layoutSubviews() {
        super.layoutSubviews()
        applyStyle()
    }

    func applyStyle() {
        let searchField = valueForKey("searchField") as? UITextField
        searchField?.backgroundColor = Theme.bgViewColor
        searchField?.leftView = UIImageView(image: UIImage(named: "icn-nav-search"))
        searchField?.textColor = Theme.textColor
        searchField?.font = UIFont(name: "ProximaNova-Light", size: 16)
        searchField?.attributedPlaceholder = NSAttributedString(string: "o que você está procurando?", attributes: [
            NSForegroundColorAttributeName: Theme.textPlaceholderColor,
            NSFontAttributeName: UIFont(name: "ProximaNova-Light", size: 16)!
            ]
        )
    }
}
