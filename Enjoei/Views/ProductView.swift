//
//  ProductView.swift
//  Enjoei
//
//  Created by Fernando Martinez on 5/8/16.
//  Copyright © 2016 fernandodev. All rights reserved.
//

import Foundation
import UIKit

class ProductView: UIView {
    @IBOutlet weak var priceTitleLabel: UILabel!
    @IBOutlet weak var discountPercentageLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var likesCountLabel: UILabel!

    //TODO: configure view presentation
}
