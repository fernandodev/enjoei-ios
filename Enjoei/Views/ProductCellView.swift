//
//  ProductCellView.swift
//  Enjoei
//
//  Created by Fernando Martinez on 5/7/16.
//  Copyright © 2016 fernandodev. All rights reserved.
//

import Foundation
import UIKit

class ProductCellView: UICollectionViewCell {

    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var likesCountLabel: UILabel!

    func configure(withTitle title:String, imageURL: NSURL?, price: Int, likesCount: Int) {
        productImageView.image = nil
        titleLabel.text = title.lowercaseString
        likesCountLabel.text = "\(likesCount)"

        let formatter = NSNumberFormatter()
        formatter.numberStyle = .CurrencyStyle
        formatter.locale = NSLocale(localeIdentifier: "pt_BR")

        let priceString = formatter.stringFromNumber(price) ?? "R$\(price)"

        priceLabel.text = priceString

        if let imageUrl = imageURL {
            productImageView.setImage(withURL: imageUrl)
        }

        applyStyle()
    }

    func applyStyle() {
        titleLabel.font = UIFont(name: Theme.fontNovaLight, size: 16)
        likesCountLabel.font = UIFont(name: Theme.fontNovaLight, size: 14)

        layer.masksToBounds = true
        layer.cornerRadius = 3

        let bottomBorder = UIView(
            frame: CGRectMake(0, frame.size.height - 3, frame.size.width, 3)
        )
        bottomBorder.backgroundColor = UIColor(red: 0.91, green: 0.90, blue: 0.89, alpha: 1)

        addSubview(bottomBorder)
    }
}