//
//  ProductsViewController.swift
//  Enjoei
//
//  Created by Fernando Martinez on 5/6/16.
//  Copyright © 2016 fernandodev. All rights reserved.
//

import UIKit

class ProductsViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var searchView: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!

    var refreshControl: UIRefreshControl?

    let api = APIProducts()
    var selectedItem: Int = 0
    var products: [Product] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.titleView = searchView

        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), forControlEvents: .ValueChanged)
        collectionView.addSubview(refreshControl)
        refreshControl.bringSubviewToFront(collectionView)
        refreshControl.beginRefreshingManually()

        self.refreshControl = refreshControl
        refresh()
    }

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let vc = segue.destinationViewController as? ProductViewController where segue.identifier == "show-product" {
            vc.product = products[selectedItem]
        }

        super.prepareForSegue(segue, sender: sender)
    }

    //TODO: Check if it is necessary
    override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        collectionView.reloadData()
        super.willRotateToInterfaceOrientation(toInterfaceOrientation, duration: duration)
    }

    //MARK: API Loading
    func refresh() {
        api.products(
            whenSuccess: { products in
                self.products = products
                self.collectionView.reloadData()
            },
            failure: { error in
                print("An error occurred \(error)")
            },
            completion: {
                self.refreshControl?.endRefreshing()
            }
        )
    }
}

//MARK: UICollectionViewDelegateFlowLayout
extension ProductsViewController {
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {

        //TODO: base width: 150 
        //TODO: Refactor via cell layout params

        let wS = UIApplication.sharedApplication().windows.first!.frame.size.width
        let hS = UIApplication.sharedApplication().windows.first!.frame.size.height
        let reference = wS < hS ? wS : hS

        let aspect = 0.52 //(w/h)
        let width = 0.4687 * reference

        return CGSizeMake(width, width/CGFloat(aspect))
    }
}

//MARK: UICollectionViewDataSource
extension ProductsViewController {

    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }

    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {

        let cell = collectionView.dequeueReusableCellWithReuseIdentifier("ProductCell", forIndexPath: indexPath)

        let product = products[indexPath.row]

        if let cell = cell as? ProductCellView {
            cell.configure(
                withTitle: product.title,
                imageURL: product.defaultPhotoUrl,
                price: product.price,
                likesCount: product.likesCount
            )
        }
        
        return cell
    }

    func collectionView(collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, atIndexPath indexPath: NSIndexPath) -> UICollectionReusableView {

        if kind == UICollectionElementKindSectionHeader {

            let header = collectionView.dequeueReusableSupplementaryViewOfKind(kind, withReuseIdentifier: "ProductHeaderCell", forIndexPath: indexPath)

            return header
        } else {
            return UICollectionReusableView()
        }
    }
}

//MARK: UICollectionViewDelegate
extension ProductsViewController {

    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        selectedItem = indexPath.row
        performSegueWithIdentifier("show-product", sender: self)
    }
}
