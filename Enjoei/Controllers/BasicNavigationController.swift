//
//  BasicNavigationController.swift
//  Enjoei
//
//  Created by Fernando Martinez on 5/6/16.
//  Copyright © 2016 fernandodev. All rights reserved.
//

import Foundation
import UIKit

class BasicNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)
        navigationBar.shadowImage = UIImage()        
    }
}