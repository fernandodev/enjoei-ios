//
//  ProductViewController.swift
//  Enjoei
//
//  Created by Fernando Martinez on 5/8/16.
//  Copyright © 2016 fernandodev. All rights reserved.
//

import Foundation
import UIKit

class ProductViewController: UIViewController {

    @IBOutlet weak var productImageView: UIImageView!
    //TODO: it will be used as children view controller container (products listing)
    @IBOutlet weak var otherProductsContainerView: UIView!

    var product: Product!

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.title = product.title.lowercaseString
        navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "icn-nav-back"), style: .Plain, target: self, action: #selector(back))

        navigationController?.navigationBar.titleTextAttributes = [
            NSFontAttributeName: UIFont(name: Theme.fontNovaBoldName, size: 18)!,
            NSForegroundColorAttributeName: Theme.textBoldColor
        ]
        tabBarController?.tabBar.hidden = true

        if let photo = product.defaultPhotoUrl {
            productImageView.setImage(withURL: photo)
        }
    }

    func back() {
        navigationController?.popViewControllerAnimated(true)
    }


}
