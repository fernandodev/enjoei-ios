![](https://jardimdoscaminhosquesebifurcam.files.wordpress.com/2015/09/enjoei.png)

Enjoei :P
==

_Enjoei :P is a simple app for job opportunity application_

_Using this application, users will be able to browse Fake Enjoei Shop._

## Getting Started

Just open

```
open Enjoei.xcodeproj
```

## Hours

|         | in     | out     | in     | out    |
|---------|--------|---------|--------|--------|
| Fri 5th | 3pm    | 5pm     | 5:30pm | 6:10pm |
| Sat 6th | 3:10pm | 7:00pm  |        |        |
| Sun 7th | 8:30am | 11:55am |        |        |

* Total: 10h33min
